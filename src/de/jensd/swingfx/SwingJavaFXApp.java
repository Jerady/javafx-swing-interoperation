package de.jensd.swingfx;

import de.jensd.swingfx.interop.InteropFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Jens Deters
 */
public class SwingJavaFXApp {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(SwingJavaFXApp::showGUI);
    }

    private static void showGUI() {
        InteropFrame frame = new InteropFrame();
        frame.setVisible(true);
        System.out.println(Thread.currentThread().getName());
    }

}
