/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.jensd.swingfx;

import de.jensd.swingfx.interop.InteropSharedModelFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author jdeters
 */
public class SwingJavaFXSharedModelApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(SwingJavaFXSharedModelApp::showGUI);
    }
    
    
    private static void showGUI(){
        InteropSharedModelFrame frame = new InteropSharedModelFrame();
        frame.setVisible(true);
    }
    
}
