package de.jensd.swingfx.interop;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javafx.application.Platform;
import javax.swing.JFrame;
import javax.swing.JSplitPane;

/**
 *
 * @author Jens Deters
 */
public class InteropFrame extends JFrame {

   
    private JSplitPane centralSplitPane;
    private SwingPanel swingPanel;
    private SwingFXMLPanel swingFXPanel;

    public InteropFrame() {
        init();
    }

    private void init() {
        setTitle("Swing <-> JavaFX Interoperatbility");
        setSize(800, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        centralSplitPane = new JSplitPane();
        swingPanel = new SwingPanel();
        swingFXPanel = new SwingFXMLPanel();
        swingPanel.getTestButton().addActionListener((ActionEvent e) -> {
            Platform.runLater(() -> {
                swingFXPanel.getTestLabel().setText(swingPanel.getTestTextField().getText());
            });
        });
        swingFXPanel.getTestButton().setOnAction(a -> {
            swingPanel.getTestLabel().setText(swingFXPanel.getTestTextField().getText());
        });
        centralSplitPane.setLeftComponent(swingPanel);
        centralSplitPane.setRightComponent(swingFXPanel);
        add(centralSplitPane, BorderLayout.CENTER);
    }

}
