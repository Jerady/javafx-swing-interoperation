package de.jensd.swingfx.interop;

import de.jensd.swingfx.model.DataModel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javax.swing.JFrame;
import javax.swing.JSplitPane;

/**
 *
 * @author Jens Deters
 */
public class InteropSharedModelFrame extends JFrame {

    private JSplitPane centralSplitPane;
    private SwingPanel swingPanel;
    private SwingFXPanel swingFXPanel;
    private SwingFXMLPanel swingFXMLPanel;

    private DataModel dataModel;

    public InteropSharedModelFrame() {
        init();
    }

    private void init() {
        setTitle("Swing <-> JavaFX Interoperatbiliy with shared data model");
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        dataModel = new DataModel();
        centralSplitPane = new JSplitPane();
        centralSplitPane.setDividerLocation(0.5);
        centralSplitPane.setResizeWeight(0.3);

        swingPanel = new SwingPanel();
        swingFXPanel = new SwingFXPanel();
        swingFXMLPanel = new SwingFXMLPanel();

        swingPanel.getTestButton().addActionListener((ActionEvent e) -> {
            dataModel.setLabelValue(swingPanel.getTestTextField().getText());
        });

        swingFXMLPanel.getTestButton().setOnAction((javafx.event.ActionEvent t) -> {
            dataModel.setLabelValue(swingFXMLPanel.getTestTextField().getText());
        });

        swingFXPanel.getTestButton().setOnAction((javafx.event.ActionEvent t) -> {
            dataModel.setLabelValue(swingFXPanel.getTestTextField().getText());
        });

        dataModel.labelValueProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            swingPanel.getTestLabel().setText(dataModel.getLabelValue());
            Platform.runLater(() -> {
                swingFXMLPanel.getTestLabel().setText(dataModel.getLabelValue());
            });
        });

        centralSplitPane.setLeftComponent(swingPanel);
        centralSplitPane.setRightComponent(swingFXMLPanel);

        add(centralSplitPane, BorderLayout.CENTER);
    }

}
