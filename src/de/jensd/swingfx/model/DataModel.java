/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.jensd.swingfx.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author jdeters
 */
public class DataModel {
    
    private StringProperty labelValueProperty;

    public StringProperty labelValueProperty() {
        if(labelValueProperty == null){
            labelValueProperty = new SimpleStringProperty();
        }
        return labelValueProperty;
    }

    public void setLabelValue(String labelValue) {
        labelValueProperty().set(labelValue);
    }

    public String getLabelValue() {
        return labelValueProperty().get();
    }
    
    
}
